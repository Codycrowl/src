/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_ADC1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
typedef enum
{
Distance_Far,
Distance_Close,
Distance_Stop,
Start
}eSystemState;
/* Prototype Event Handlers */
// Based on Sharp GP2Y0A60SZ0F/GP2Y0A60SZLF voltages
eSystemState GreenHandler(void)
{
HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY);
uint16_t rawValue;
float voltage;
rawValue = HAL_ADC_GetValue(&hadc1);
voltage =((float)rawValue)/4095 * 3.3;
HAL_GPIO_WritePin(GPIOA, Green_P_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA, Yellow_P_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, Red_P_LED_Pin, GPIO_PIN_RESET);

//HAL_Delay(5*1000); //10 seconds

if(voltage > .75)//approx. 30cm
		  {
			return Distance_Stop;
		  }
		  else if(voltage < .75)//approx. 50cm
		  {
			  if (voltage < .4)//approx. 100cm
			  {
				  return Distance_Far;
			  }
			  else
			  {
				  return Distance_Close;
			  }
		  }
}
eSystemState YellowHandler(void)
{
HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY);
uint16_t rawValue;
float voltage;
rawValue = HAL_ADC_GetValue(&hadc1);
voltage =((float)rawValue)/4095 * 3.3;
HAL_GPIO_WritePin(GPIOA, Green_P_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, Yellow_P_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA, Red_P_LED_Pin, GPIO_PIN_RESET);
//HAL_Delay(3*100); //5 seconds


if(voltage > .75)//approx. 30cm
		  {
			return Distance_Stop;
		  }
		  else if(voltage < .75)//approx. 50cm
		  {
			  if (voltage < .4)//approx. 100cm
			  {
				  return Distance_Far;
			  }
			  else
			  {
				  return Distance_Close;
			  }
		  }
}
eSystemState RedHandler(void)
{
HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY);
uint16_t rawValue;
float voltage;
rawValue = HAL_ADC_GetValue(&hadc1);
voltage =((float)rawValue)/4095 * 3.3;
HAL_GPIO_WritePin(GPIOA, Green_P_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, Yellow_P_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, Red_P_LED_Pin, GPIO_PIN_SET);
//HAL_Delay(2*100); //5 seconds


if(voltage > .75)//approx. 30cm
		  {
			return Distance_Stop;
		  }
		  else if(voltage < .75)//approx. 50cm
		  {
			  if (voltage < .4)//approx. 100cm
			  {
				  return Distance_Far;
			  }
			  else
			  {
				  return Distance_Close;
			  }
		  }
}
/**
  * @brief  The application entry point.
  * @retval int
  */
/* USER CODE END 0 */
  //Proximity Sensor
	  HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY);
	  rawValue = HAL_ADC_GetValue(&hadc1);
	  voltage =((float)rawValue)/4095 * 3.3; // voltage = (rawVoltage/(2^bit resolution-1)*Vref)
	  sprintf(msg, "rawValue: %hu\r\n", rawValue);
	  HAL_UART_Transmit(&huart2, (uint8_t*) msg, strlen(msg), HAL_MAX_DELAY);
	  sprintf(msg, "voltage: %f\r\n", voltage);
	  HAL_UART_Transmit(&huart2, (uint8_t*) msg, strlen(msg), HAL_MAX_DELAY);

 //Spot Sensor
      if(HAL_GPIO_ReadPin(GPIOB, Spot_Sensor_Pin))
                  {
                    HAL_GPIO_WritePin(GPIOB, Red_S_LED_Pin, GPIO_PIN_SET);
                    HAL_GPIO_WritePin(GPIOB, Green_S_LED_Pin, GPIO_PIN_RESET);
                  }
                  else
                  {
                    HAL_GPIO_WritePin(GPIOB, Red_S_LED_Pin, GPIO_PIN_RESET);
                    HAL_GPIO_WritePin(GPIOB, Green_S_LED_Pin, GPIO_PIN_SET);
                  }
                  HAL_Delay(100);


  //Proximity Sensor
	  switch(eNextState)
	  {
	  /*case Start:
		  eNextState = RedHandler();
		  //HAL_Delay(2*1000);
	  break;*/
	  case Distance_Far:
		eNextState = GreenHandler();
	  break;

	  case Distance_Close:
		eNextState = YellowHandler();
	  break;

	  case Distance_Stop:
		eNextState = RedHandler();
	  break;

	  }
